# Creating an EKS cluster using GitLab

GitLab offers integrated cluster creation for EKS, which makes the creation of an EKS cluster very easy. You will need the following to accomplish this:

1. A valid GitLab account
2. A valid AWS account

## Using the GitLab integrated cluster creation

You can create a Kubernetes cluster at the [group level](https://docs.gitlab.com/ee/user/group/clusters/) or project level within GitLab. For this lab, we are going to create a Kubernetes cluster at the project level. To this end, you first need to create a project.

### Create a project

Log on to your GitLab workspace and create a new project:

---

<img src="images/auto-deploy/1-click-new-proj.png" width="75%" height="75%">

---

Click on the *New project* blue button, which will take you to the *Create new project* screen:

> **NOTE:** if this is your first time logging in to your GitLab account, you will get Welcome screen with a few tiles. Click on the *Create a project* tile.

---

<img src="images/auto-deploy/2-click-on-import-proj.png" width="50%" height="50%">

---

Click on the *Import project* tile to go to the *Import project* screen:

---

<img src="images/auto-deploy/3-click-on-repo-by-url.png" width="50%" height="50%">

---

Click on the *Repo by URL* button, which will expand the screen showing fields related to the project to be imported. Enter the following project URL into the **Git repository URL** field:

> https://gitlab.com/tech-marketing/sandbox/simple-java.git

and ensure that Public is checked for **Visibility Level**. Your screen should look similar to the following one:

---

<img src="images/auto-deploy/4-enter-proj-url.png" width="50%" height="50%">

---

Click on the *Create project* green button at the bottom of the screen to import the project. You will see a temporary *Import in progress* message on your screen:

---

<img src="images/auto-deploy/5-import-in-progress.png" width="50%" height="50%">

---

After the import is finished, you will be put into your newly created project screen, showing its contents:

---

<img src="images/auto-deploy/6-imported-proj-dir.png" width="75%" height="75%">

---

### Create a project-level EKS cluster

> **NOTE:** if you have already created a group-level EKS cluster for all your lab projects, then SKIP THIS SECTION!

On the left margin of your project window, click on **Operations > Kubernetes** to go to the Kubernetes screen:

---

<img src="images/auto-deploy/7-K8s-screen.png" width="50%" height="50%">

---

Click on the *Integrate with a cluster certificate* green button to go to the **Add a Kubernetes cluster integration** screen:

---

<img src="images/auto-deploy/8-click-on-create-EKS.png" width="75%" height="75%">

---

On this screen you can see tiles for GitLab's integrated cluster creation options. Since this lab is about creating an EKS cluster, click on the **Amazon EKS** tile to expand the screen with the input fields needed by the integration for the creation of an EKS cluster:

---

<img src="images/auto-deploy/9-enter-your-role-ARN.png" width="75%" height="75%">

---

In the above screen, you will see the default **Account ID** and **External ID**, which are readonly. You need to enter your **Provision Role ARN** (the string *arn:aws:iam::[AWS ACCOUNT]:role/[IAM USER]* is just an example of its syntax and not a real Provision Role ARN). Besides the **Provision Role ARN**, you will also need a **Service role**, **Key pair name**, and **VPC** with its subnets and security group. To create all these, follow these [instructions](subchapters/create-aws-roles-for-eks.md).

If you're back at this point, it means that you have created all the AWS resources needed by the GitLab integrated EKS cluster creation. So, enter your provision role ARN in the field **Provision Role ARN**. For the **Cluster region** field, enter the same region you used for the creation of the required cluster **VPC**, or you can leave it blank if you created your **VPC** in the us-east-2 (Ohio) region, which is the default for this field.

Click on the *Authenticate with AWS* button at the bottom of the screen. This will take you to the **Enter the details for your Amazon EKS Kubernetes cluster** window. On this window, enter the following values:

| Field | Value | Notes
| ----------- | ----------- | ----------- |
| Kubernetes cluster name | [YOUR GITLAB ID]-eks-cluster | you can name it whatever you'd like but we recommend adding your GitLab handle in front so that you can easily find your cluster during a search. Replace *[YOUR GITLAB ID]* with your GitLab handle
| Environment scope | * | indicates that this cluster will be used for all environments you create within this project
| Kubernetes version | leave its default value
| Service role | eksClusterRole | if you followed the instructions, this should be the name for the service role
| Cluster Region | readonly and cannot change
| Key pair name | enter the one you created | if you followed the instructions, this should be *[YOUR GITLAB ID]-key-pair*
| VPC | enter the one you created | click on the field to open the search and type in the name of the VPC you created. If you followed the instructions, this should be *[YOUR GITLAB ID]-eks-vpc-VPC*
| Subnets | the three subnets you created | click on the field to open the search and the list of all your subnets. Select the ones you created one at a time and ensure that all three are entered in this field
| Security group | the name of the security group you created | click on the field to open the search and the list of all your security groups. If you followed the instructions, you should see a security group name *[YOUR GITLAB ID]-eks-vpc-ControlPlaneSecurityGroup-[long alphanumeric string]* in the list. Select this one. If you're not sure which security group, go to your **AWS Console > VPC Dashboard > Security > Security Groups** and locate the **Security group ID** for the security group you created. Its name will be in the column **Security group name**
| Instance type | m5.large | 2 vCPUs and 8GB RAM
| Number of nodes | 3 | good number for 1 master node and 2 worker nodes
| GitLab-managed cluster | checked | we want GitLab to manage this cluster
| Namespace per environment | checked | indicates that every environment will use its own namespace

Once you have populated all the fields in the **Enter the details for your Amazon EKS Kubernetes cluster** window, scroll to the bottom of the screen and click on the *Create Kubernetes Cluster* green button:

---

<img src="images/auto-deploy/38-click-on-create-cluster.png" width="75%" height="75%">

---

You will be put in the **Project cluster** window and you will see the following message while the EKS cluster is being created:

---

<img src="images/auto-deploy/39-cluster-being-created.png" width="30%" height="30%">

---

After 10-15 minutes, the creation process will finish and your newly created cluster window will be displayed:

---

<img src="images/auto-deploy/40-eks-cluster-details.png" width="75%" height="75%">

---

Click on the *Applications* tab to see of the applications that you can deploy to the running EKS cluster with a click of a button:

---

<img src="images/auto-deploy/41-click-on-app-and-install.png" width="75%" height="75%">

---

Click on the *Install* buttons for the *Ingress*, *Cert-Manager* and *Prometheus* applications to get them installed on your newly created cluster. Wait until all three applications are installed. You will know that they are all installed when their *Install* button are re-labeled *Uninstall*.

Copy and save the value for the *Ingress Endpoint*, which was populated when the *Ingress* application was successfully installed. Here's a screenshot of the *Ingress Enpoint* that I got (yours will be different):

---

<img src="images/auto-deploy/42-copy-ingress-endpoint.png" width="75%" height="75%">

---

You now need to create a DNS CNAME (alias) record for this ingress endpoint with a public DNS service. I used Google Cloud DNS but you can use any other one, e.g. AWS Route53. You need to create two CNAME records, one for the domain and one for the wildcard domain. I'm using the domain **tmm.tanuki.host**, which I already own (the domain that you own will be different). Here's a screenshot of the domain CNAME record creation:

---

<img src="images/auto-deploy/43-create-DNS-entry.png" width="40%" height="40%">

---

and here's a screenshot of the wildcard domain CNAME record creation:

---

<img src="images/auto-deploy/44-create-wildcard-DNS-entry.png" width="40%" height="40%">

----
**NOTE:** You can find more examples of how to set DNS CNAME (alias) records with different cloud DNS services [here](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/subchapters/examples-for-setting-DNS-CNAME-record.md).

The last thing you need to do is to set your newly create EKS cluster base domain. Back on the EKS cluster detail window, click on the *Details* tab:

---

<img src="images/auto-deploy/45-set-base-domain.png" width="50%" height="50%">

---

In the *Base domain* field above, paste the public domain you used in your CNAME record. In my case, it was *tmm.tanuki.host*:

That's it! The EKS cluster is now all set to be used for deployments in your project.

Now you can head back to the *[Using Auto Deploy for K8s](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/10.%20Auto%20Deploy.md#using-auto-deploy-for-k8s)* lab.
