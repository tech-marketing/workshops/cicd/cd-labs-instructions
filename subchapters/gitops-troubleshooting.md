# GitOps troubleshooting

## Validate stage (**validate** job) fails for pipeline located at gitops > infra > aws

In the case that the **validate** job fails in the pipeline for project **gitops > infra > aws** fails as shown below:

---

<img src="images/gitops-troubleshooting/2-infra-aws-pipeline-validate-failed.png" width="75%" height="75%">

---

We provide some steps on how to troubleshoot it. Click on the failed **validate** job to see its log output:

---

<img src="images/gitops-troubleshooting/3-missing-required-arg-error.png" width="50%" height="50%">

---

The error above indicates that there was a syntactical error in one of the Terraform configuration files. You will need to run **terraform fmt** on these files. In order to do this, you will need to know what version of Terraform GitLab is running. Scroll towards the beginning of the log output file of the failed **validate** job until you see a line that says:

> $ terraform --version

---

<img src="images/gitops-troubleshooting/4-terraform-version.png" width="50%" height="50%">

---

In the above screenshot, Terraform v0.14.10 is the version running on this GitLab instance. Please make note of this because you will need it in the next section.

### Fixing the formatting of the Terraform configuration files

You will be running the command **terraform fmt** from your laptop on the cloned Terraform configuration files.

1. First, open a Terminal window on your local laptop/desktop and ensure that whatever version of Terraform you're running on your laptop is the same as the one that GitLab is running. Here are the [detailed instructions](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/subchapters/installing-terraform.md) to download, install Terraform and connect to the Terraform state file managed by GitLab for the **aws** project (when done, press the Back button on your browser to return to this page).

2. Copy the URL to clone the project to your local laptop. In the screenshot below, we are copying the URL string to clone the project **gitops > infra > aws***:

---

<img src="images/gitops-troubleshooting/5-copy-clone-with-ssh-string.png" width="50%" height="50%">

---

3. Open a Terminal (Command) window on your laptop and enter the following commands:

```
cd $HOME
git clone <REPLACE WITH THE COPIED CLONE URL>
```

Here's an example of the execution of the commands above:

---

<img src="images/gitops-troubleshooting/6-git-clone.png" width="50%" height="50%">

---

4. Now, you need to change directory into the cloned project and run **terraform fmt** in it. Terraform will inspect all Terraform configuration files (all files with .tf file extension) in the project and correct any formatting. For example, if the cloned project was **aws**, here are the commands you'd need to enter:

```
cd aws
terraform fmt
```

5. After **terraform fmt** is done executing, run git status to get the list of the files that **terraform fmt** modified:

```
git status
```

> **NOTE:** Notice that the **git status** command will return not only the files that **terraform fmt** modified, but also any new files in the directory which includes the **.terraform.lock.hcl** lock file. We **do not** want to upload the lock file to our GitLab **aws** project so we must skip it during the **commit*.

6. Add only the modified Terraform configuration files (files with **.tf** file extension) to Git tracking. For example, if the only file that **terraform fmt** modified was **vpc.tf**, then this is the **add** command you would enter (if more than 1 Terraform file was modified then list them, separated by a space, right after **git add**):

```
git add vpc.tf
```

7. Execute a **git commit**, such as:

```
git commit -m "ran terraform fmt"
```

8. Execute a **git push**:

```
git push
```
Here's an example of the execution of the commands executed in steps 4-8 above:

---

<img src="images/gitops-troubleshooting/7-run-terraform-fmt.png" width="50%" height="50%">

---

This last command should launch a new pipeline with the correctly formatted Terraform configuration files.

## Plan stage fails (**plan production** job) fails for pipeline located at gitops > infra > aws

In the case that the **plan production** job fails in the pipeline for project **gitops > infra > aws** fails as shown below:

---

<img src="images/gitops-troubleshooting/8-plan-prod-job-failed.png" width="75%" height="75%">

---

Here are a couple of reasons of why this happens and steps on how to fix the problem.

### Bad location of apps subgroup

Click on the failed **plan production** job to see its log output:

---

<img src="images/gitops-troubleshooting/8.1-plan-prod-error-bad-proj-name.png" width="50%" height="50%">

---

The error above indicates that the group **apps** subgroup is not found. Follow these steps to troubleshoot:

1. Navigate to the **gitops > apps** subgroup using the breadcrumb at the top of your GitLab workspace:

<img src="images/gitops-troubleshooting/10-apps-subgroup-URL.png" width="50%" height="50%">

2. Inspect the URL field of your browser to see the actual location of the **apps** subgroup. In the screenshot above, the actual location is **gitops12/apps**, not **gitops/apps** as shown in the log. This is the cause of the error. Let's go ahead and fix it.

3. Navigate to the project **gitops > infra > aws** and inspect line 2 of the file **group_cluster.tf**, which should look similar to the following:

>   full_path = "gitops/apps"

Ensure that the value of the variable **full_path** is the correct one. In this example, it is incorrect because the actual URL of the **apps** subgroup is **gitops12/apps**. The correction that you'd need to make by editing its line 2 of the **group_cluster.tf** file would be:

>   full_path = "gitops12/apps"

4. The update should kick off a new pipeline. If the **validate** job fails, you may need to re-run **terraform fmt** on all the Terraform configuration files by following the [instructions in the first section above](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/subchapters/gitops-troubleshooting.md#fixing-the-formatting-of-the-terraform-configuration-files).

### Incomplete procurement of infrastructure

Click on the failed **plan production** job to see its log output:

---

<img src="images/gitops-troubleshooting/8.2-plan-prod-job-failed-log.png" width="50%" height="50%">

---

The error above may be an indication that a previous deployment of the infrastructure may have left things incomplete. In other words, the Terraform state file and the running infrastructure may not match. To fix this, you need to wipe out the Terraform state file. Follow these steps to accomplish this:


1. Navigate to the **gitops > infra > aws** project (if you're not already there) and select **Operations > Terraform** from the left window menu:
---

<img src="images/gitops-troubleshooting/8.3-ops-terraform.png" width="20%" height="20%">

---

2. On the Terraform window, you will see a Terraform state file for the project **aws**. Click on three dots under the **Actions** header to display the popdown menu. Select the **Remove state file and versions** entry.

---

<img src="images/gitops-troubleshooting/8.4-rem-terra-state-file.png" width="50%" height="50%">

---

6. A confirmation pop-up will appear. Enter **aws** in the field **To remove the State file and its versions, type aws to confirm:** and click on the **Remove** button:

---

<img src="images/gitops-troubleshooting/8.5-rem-terra-state-file-confirm.png" width="50%" height="50%">

---

7. Once the Terraform state file is removed, you need to rerun the pipeline. Select **CI/CD > Pipelines** from the left side menu to go to the Pipelines window:

---

<img src="images/gitops-troubleshooting/8.6-clear-runner-caches.png" width="50%" height="50%">

---

On the Pipelines window, click on the **Clear runner caches** button on the top right.

8. Once all runner caches are clear, run the pipeline on the **master** branch.

## Apply stage fails (apply job) fails for pipeline located at gitops > infra > aws

In the case that the **apply** job fails in the pipeline for project **gitops > infra > aws** fails as shown below:

---

<img src="images/gitops-troubleshooting/9-apply-job-failed.png" width="75%" height="75%">

---

We provide some guidance as to how to resolve them. Errors in this stage are usually related to AWS resource constraints that can only be detected when procurement takes place. The errors may be due to previous unsuccessful infrastructure deployment that left AWS resources laying around in mid-creation. For example:

A. The storage size for the database is invalid

B. The minimum auto scaling group number is not 1

C. An IAM (Identity and Access Management) role already exists

Correction of the error will get rid of the error. For example:

A. The storage size for the database is invalid. **Solution:** set the storage type for the database to a correct type in file **mysqlmain.tf**

B. The minimum auto scaling group number is not 1. **Solution:** set the asg_min_size variable to 1 in **eks.tf**

C. An IAM (Identity and Access Management) role already exists. **Solution:** delete the role in the AWS AIM console. For example, **mysqlmain.tf** creates a role **MyRDSMonitoringRole** on your behalf. If a previous deployment failed, this role may have already been created and would need to be deleted before a new Gitops deployment is launched.

## Deploy stage does not execute (deploy-apps job) for pipeline located at gitops > infra > aws

In the case that the **deploy-apps** does not execute and does not respond to clicking on its **Start** button in the pipeline for project **gitops > infra > aws** fails as shown below:

---

<img src="images/gitops-troubleshooting/11-deploy-apps-job-failed.png" width="75%" height="75%">

---

The most likely culprit is that the setting of the cluster management project for the EKS cluster failed. To solve this, you need to manually set it. Let's do it now.

1. First, you need to get to the K8s window. The EKS cluster is a group cluster located in the **apps** subgroup under your **gitops** top-level group. Group cluster are visible/accessible from any projects or subgroups with a matching environment scope (with the caveat that project clusters override group clusters):

---

<img src="images/gitops/26-gitops-group-struct.png" width="60%" height="60%">

---

Looking at the picture above, the EKS cluster was created at the **apps** subgroup level, so it is accessible from the **Cluster Management** and the **Spring MVC JPA** projects. To get to the GitLab Kubernetes window that lists the EKS cluster, navigate to the **aws** group by using the breadcrumb at the top of your window. First click on the **gitops** component towards the beginning of the breadcrumb, and then click on the **apps** subgroup. Once in the **apps** subgroup, select **Kubernetes** from its left menu:

---

<img src="images/gitops-troubleshooting/1-K8s-menu-entry.png" width="40%" height="40%">

---

2. As a result of the previous step, you will be on the Kubernetes window, which displays the EKS cluster entry:

---

<img src="images/gitops/32-click-on-eks-cluster.png" width="75%" height="75%">

---

Click on the name of the EKS cluster.

3. Once inside the EKS Kubernetes integration window, select the **Advanced Settings** tab:

---

<img src="images/gitops/33-select-cluster-mgmt-proj.png" width="75%" height="75%">

---

Click on the down-arrow at the end of the **Cluster management project (alpha)** field, to display a popdown list of all projects in scope. Select the entry for the **Cluster Management** project in the **apps** subgroup.

4. Click on the **Save changes** button.

5. Head back over to your subgroup **gitops > infra > aws** by using the breadcrumb at the top of your window.

6. Once at the **gitops > infra > aws** subgroup, select **CI/CD Pipelines** from its left menu.

7. Click on the **Run Pipeline** button on the top right of your screen and ensure that **master** is selected for the **Run for branch name or tag** popdown menu. Then click on the **Run Pipeline** button.

## Downstream stage (cluster-management job) fails for pipeline located at gitops > infra > aws

In the case that the Downstream stage in the pipeline for project **gitops > infra > aws** fails as shown below:

---

<img src="images/gitops/30-infra-failed-cluster-mgmt-job.png" width="75%" height="75%">

---

We provide some steps on how to troubleshoot it. Click on the **>** symbol in the **Downstream** stage to expand it. You will see the failed **eks-install** job in the **Deploy** stage:

---

<img src="images/gitops/31-apps-failed-eks-install-job.png" width="40%" height="40%">

---

If you click on the failed eks-install job above, you will see its log output:

---

<img src="images/gitops/31.1-failed-eks-install-log.png" width="50%" height="50%">

---

The error will be related to the inability to create a namespace due to lack of the appropriate permissions. The most likely culprit is that the setting of the cluster management project for the EKS cluster failed. To solve this, you need to manually set it. Let's do it now.

#### Setting the cluster management project for EKS cluster

1. First, you need to get to the K8s window. The EKS cluster is a group cluster located in the **apps** subgroup under your **gitops** top-level group. Group cluster are visible/accessible from any projects or subgroups with a matching environment scope (with the caveat that project clusters override group clusters):

---

<img src="images/gitops/26-gitops-group-struct.png" width="60%" height="60%">

---

Looking at the picture above, the EKS cluster was created at the **apps** subgroup level, so it is accessible from the **Cluster Management** and the **Spring MVC JPA** projects. To easily get to the GitLab Kubernetes window that lists the EKS cluster, you have a few options:

**A.** From the browser window displaying the log out of the failed eks-install job, you can access the Kubernetes window by selecting **Operations > Kubernetes** because the eks-install job is in the cluster-management project, which has visibility to the group EKS cluster you need to get to.

Or,

**B.** You can navigate to the **aws** group by using the breadcrumb at the top of your window. First click on the **gitops** component towards the beginning of the breadcrumb, and then click on the **apps** subgroup. Once in the **apps** subgroup, select **Kubernetes** from its left menu:

---

<img src="images/gitops-troubleshooting/1-K8s-menu-entry.png" width="40%" height="40%">

---

2. As a result of the previous step, you will be on the Kubernetes window, which displays the EKS cluster entry:

---

<img src="images/gitops/32-click-on-eks-cluster.png" width="75%" height="75%">

---

Click on the name of the EKS cluster.

3. Once inside the EKS Kubernetes integration window, select the **Advanced Settings** tab:

---

<img src="images/gitops/33-select-cluster-mgmt-proj.png" width="75%" height="75%">

---

Click on the down-arrow at the end of the **Cluster management project (alpha)** field, to display a popdown list of all projects in scope. Select the entry for the **Cluster Management** project in the **apps** subgroup.

4. Click on the **Save changes** button.

### Retrying the pipeline for the infra group

5. Now that we have fixed the cluster management project for the EKS cluster, let's try rerunning the failed eks-install job. From the pipeline view containing the failed eks-install job, click on the **Retry** icon:
---

<img src="images/gitops/34-click-retry-eks-install-job.png" width="40%" height="40%">

---

Once the job successfully finishes, you should see a green checkmark next to it indicating its successful execution has ended:

---

<img src="images/gitops/35-click-on-eks-install.png" width="40%" height="40%">

---
