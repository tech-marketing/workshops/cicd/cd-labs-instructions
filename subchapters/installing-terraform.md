# Installing Terraform on your laptop/desktop

The following instructions go over the steps to download and install Terraform on your laptop/desktop (using MacBook Pro). They also show how to connect to the Terraform state file stored and managed by GitLab.

## Downloading and installing Terraform

1. If you already have Terraform installed on your laptop then you can just check its version to see if it is the one you need to work with your GitLab Gitops project. If this is the first time you're installing Terraform on your laptop then skip this step. To check the version of your installed Terraform, open a Terminal (Command) window on your laptop and enter the following command:

> terraform -version

Here's an example screenshoot of what this command returns:

<img src="images/terraform/1-terraform-version-cmd-line.png" width="50%" height="50%">

> **NOTE:** GitLab may be running a version that is not the latest version of Terraform. It is recommended to use/install on your laptop the same version of Terraform that GitLab is using. So, you may ignore the **out of date!** warning message that the Terraform command returns, if this is your case.

If this is the version that you need to work with your GitLab Gitops project, then skip the rest of these instructions.

2. If this is the first time you're installing Terraform, then you need to first download it. Open a browser window and enter the following URL to head to the download site:

> https://www.terraform.io/downloads.html

<img src="images/terraform/2-terraform-download.png" width="50%" height="50%">

For this example, we're using a MacBook Pro, so click on the *64-bit* under **macOS**. This will download a **zip** file to your **Downloads** folder. Here's an example of a Terraform zip file in my Downloads folder:

<img src="images/terraform/3-terraform-in-downloads.png" width="50%" height="50%">

3. On your Terminal (Command) window (if you don't have one open, please open one now), enter the following commands to create a brand new directory in your $HOME directory and unzip the downloaded terraform zip file in the newly created directory (please substitute the name of the downloaded Terraform file with the one you downloaded):

```
cd $HOME
mkdir terraform_0.14.10
cd terraform_0.14.10
unzip $HOME/Downloads/terraform_0.14.10_darwin_amd64.zip
```

Here is an example screenshot of the execution of the commands above:

<img src="images/terraform/4-terraform-unzip.png" width="50%" height="50%">

At this point, you have installed Terraform in its own directory under your $HOME directory.

4. Now you need to update your login profiles so that every time you open a Terminal (Command) window, your environment will be properly set to invoke the correct version of Terraform. We usually use either the Mac Z-shell (zsh) or bash so we will update the profiles for these two shells. To update the Z-shell, head to your $HOME directory and edit (I used **vi**) the **.zprofile** file and add (or update) to your PATH environment variable the location of the Terraform executable. Here are the commands to go to your $HOME directory and start the edit:

```
cd $HOME
vi .zprofile
```

This is how you would add the Terraform location to your PATH environment variable inside the **.zprofile** file (I'm adding the terraform_0.14.10 location in this example):

> export PATH=$PATH:$HOME/terraform_0.14.10

Here's a snapshot of the **.zprofile** file being edited:

<img src="images/terraform/5-edit-zprofile.png" width="50%" height="50%">

Once you have made the update, save the file. In vi, you press the ESC key to exit edit mode and then press the "**:**" character followed by the characters "**wq**" to write the file and quit the edit session.

5. If you don't use bash then skip this step.  To update bash, head to your $HOME directory and edit (I used **vi**) the **.bash_profile** file and add (or update) to your PATH environment variable the location of the Terraform executable. Here are the commands to go to your $HOME directory and start the edit:

```
cd $HOME
vi .bash_profile
```

This is how you would add the Terraform location to your PATH environment variable inside the **.bash_profile** file (I'm adding the terraform_0.14.10 location in this example):

> export PATH=$PATH:$HOME/terraform_0.14.10

Here's a snapshot of the **.bash_profile** file being edited:

<img src="images/terraform/6-edit-bash-profile.png" width="50%" height="50%">

Once you have made the update, save the file. In vi, you press the ESC key to exit edit mode and then press the "**:**" character followed by the characters "**wq**" to write the file and quit the edit session.

6. Let's verify that you can invoke Terraform from your Terminal (Command) window. To make things simple, go ahead and close the Terminal (Command) window from which you modified the login profiles for the Z-shell and bash shell. Then open a brand new Terminal (Command) window. From the newly opened Terminal (Command) window, enter the following command:

> terraform -version

This command will return the version of the newly installed (updated) Terraform. Here is an example of a screenshot of executing this command right after Terraform v0.14.10 had been installed on my laptop:

<img src="images/terraform/7-terraform-version-updated.png" width="50%" height="50%">

The return value should match the version of the Terraform you need to start working with your GitLab Gitops files.

## Connecting to the Terraform state file in your GitLab project

Before you start using Terraform on your GitLab Gitops project files, you need to connect to the Terraform state file stored and managed in your GitLab Gitops project. In this example, we are going to connect to the GitLab Gitops project **aws**.

> **NOTE:** you need to tailor the following instructions to your specific GitLab Gitops project

7. Let's create a script with the **Terraform init** command in it. Head over to your $HOME directory and create a brand new script **terraInit.sh** by entering the following commands from a Terminal (Command) window ((if you don't have one open, please open one now)):

```
cd $HOME
touch terraInit.sh
```

8. Edit the **terraInit.sh** file using your favorite editor and copy and paste the following lines:

> **NOTE:** you need to replace the strings `PROJECT_ID`, `GITLAB_HANDLE`, and `GITLAB_TOKEN` in the code segment below, where `PROJECT_ID` is the **PROJECT ID** of your GitLab Gitops project, GITLAB_HANDLE is your GitLab username, and GITLAB_TOKEN is your personalized API Token for your GitLab account (created via GitLab UI, **User Settings > Access Tokens > Create personal access token** and ensuring the token scope is "api"). Notice that **aws** is the name of the GitLab Gitops project name; if your project has a different name, then you must also replace **aws** with your project name.

```
# The password value in the command is the Gitlab API Token
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/PROJECT_ID/terraform/state/aws" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/PROJECT_ID/terraform/state/aws/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/PROJECT_ID/terraform/state/aws/lock" \
    -backend-config="username=GITLAB_HANDLE" \
    -backend-config="password=GITLAB_TOKEN" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```

Once you have made the updates, save the file. In vi, you press the ESC key to exit edit mode and then press the "**:**" character followed by the characters "**wq**" to write the file and quit the edit session.

9. We need to execute the script from the previous step from inside your cloned project on your laptop. In this example, the cloned project is **aws** so here are the instructions to run the script from the cloned **aws** directory:

```
cd aws
. ../terraInit.sh
```

Here's an example of the output of a successful run on the script above:

<img src="images/terraform/8-terra-init-output.png" width="50%" height="50%">

You have completed the installation (or update) of Terraform on your laptop/desktop and have connected to your GitLab Gitops project Terraform state file.
