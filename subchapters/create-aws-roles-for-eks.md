# Creating AWS policy and roles

To be able to use GitLab's integration Kubernetes cluster creation for EKS, using your AWS account, you first need to create:

1. a policy
2. a provision role ARN
3. a service role
4. a VPC, and
5. a key pair - you can have AWS create a key pair for you, OR if you already have your own key pair, e.g. public and private keys in your $HOME/.ssh directory, you can import them onto AWS. To create your key pair, follow these [instructions](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html#having-ec2-create-your-key-pair)

These are the steps to accomplish the first 4 items from the list above. You can also find these instructions in our [documentation](https://docs.gitlab.com/ee/user/project/clusters/add_eks_clusters.html#new-eks-cluster). 

## Creating a full access policy

This subsection creates a policy that will give GitLab the full access permissions to create and manage resources. If you'd like to create a policy that only gives GitLab the permissions to be able to create resources, but not delete them, then go [here](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/subchapters/create-non-full-access-policy.md).

Log on to the [AWS IAM Management Console](https://console.aws.amazon.com/iam/home) using your AWS credentials. Once logged on, in the navigation panel on the left, click on **Access management > Policies**:

<img src="images/aws/10-click-accessmgmt-policies.png" width="20%" height="20%">

On the next screen click on the *Create Policy* blue button:

<img src="images/aws/11-click-on-create-policy.png" width="75%" height="75%">

On the **Create Policy**, click on the *JSON* tab and paste the following text into it:

```
{
     "Version": "2012-10-17",
     "Statement": [
         {
             "Effect": "Allow",
             "Action": [
                 "autoscaling:*",
                 "cloudformation:*",
                 "ec2:*",
                 "eks:*",
                 "iam:*",
                 "ssm:*"
             ],
             "Resource": "*"
         }
     ]
}
```

The text above will give GitLab full access permissions to the resources. Click on the *Next: Tags* blue button at the bottom to go to the **Add tags (Optional)** screen:

<img src="images/aws/13-add-tag-click-review.png" width="50%" height="50%">

Although optional, it's always useful to add a tag so that you can search for AWS resources you created. You can enter any key-value pair you want but I always like to enter the *gitlabuser* as the **Key** and my GitLab handle as the **Value**. In the screenshot above, you would need to replace the string **YOUR GITLAB ID** with your own GitLab handle. Click on the *Next: Review* blue button to move on to the **Review Policy** window:

<img src="images/aws/54-create-fullaccess-policy.png" width="75%" height="75%">

In the screenshot above, replace **YOUR-GITLAB-ID** with your own GitLab handle. I have chosen to add the suffix *-fullaccess-policy* to the name of the policy, which describes what the policy is for. You can choose to name it anything you want, if you prefer. Click on the *Create policy* blue button at the bottom to create the policy.

## Creating the Provisioning Role ARN that uses the full access policy

Also known as the Provisioning Role ARN, this is the role that will authenticate to AWS and be the creator of the EKS cluster.

Go to the [IAM Console](https://console.aws.amazon.com/iam/) and log in using your AWS credentials. Once logged on, in the navigation panel on the left, click on **Access management > Roles**:

<img src="images/aws/15-click-accessmgmt-roles.png" width="20%" height="20%">

On the next screen, click on the *Create role* blue button:

<img src="images/aws/16-click-on-create-role.png" width="75%" height="75%">

On the **Create role** screen, click on **Another AWS account** tile to display the *Specify accounts that can use this role* fields. In the **Options**, click the *Require external ID (Best practice when a third party will assume this role)* checkbox to show the **External ID** input field. Now, enter the **Account ID** and **External ID** values from your Amazon EKS Cluster creation screen from GitLab (from [this screen](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/raw/master/images/auto-deploy/9-enter-your-role-ARN.png) from [this lab](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/subchapters/create-group-eks-cluster.md)):

<img src="images/aws/22-enter-account-external-ids.png" width="50%" height="50%">

Click on the *Next: Permissions* blue button at the bottom of the screen. In the **Attach permissions policies** window, search for the name of the policy you created above in the first section of this lab. For example, I named mine *csaavedra-fullaccess-policy* (yours will be different) so I entered *csaa* in the **Filter policies** field to find the policy I had created as the first step of this lab. Once the policy is found, click on its checkbox to select it:

<img src="images/aws/55-attach-fullaccess-to-ARN.png" width="75%" height="75%">

Click on the *Next: Tags* blue button at the bottom. You will see the following screen:

<img src="images/aws/24-add-tag-click-review.png" width="50%" height="50%">

Although optional, it's always useful to add a tag so that you can search for AWS resources you created. You can enter any key-value pair you want but I always like to enter the *gitlabuser* as the **Key** and my GitLab handle as the **Value**. In the screenshot above, you would need to replace the string **YOUR GITLAB ID** with your own GitLab handle. Click on the *Next: Review* blue button:

<img src="images/aws/25-name-role-arn-click-create.png" width="75%" height="75%">

In the screenshot above, replace **YOUR-GITLAB-ID** with your own GitLab handle. I have chosen to add the suffix *-provision-role-ARN* to the name of the role, which describes what the role is for. You can choose to name it anything you want, if you prefer. Click on the *Create role* blue button at the bottom to create the provision role ARN.

Once your provision role ARN is created, you will see the following message:

<img src="images/aws/26-role-arn-created-msg.png" width="50%" height="50%">

I named my provision role ARN *csaavedra-provision-role-ARN* (yours will be different). The name of your newly created provision role ARN in the message above is a link. Click on it to go to the role detail page. You will see a screen similar to the following one:

<img src="images/aws/27-role-arn-detail-redacted.png" width="50%" height="50%">

We have redacted portions of the fields in the screenshot above to cover specific account information. The field that you will need to copy to paste into the Create EKS screen on GitLab is the **Role ARN** above. Go ahead and copy it to your clipboard or to a file on your local computer now.

## Creating the service role

Kubernetes clusters managed by Amazon EKS make calls to other AWS services on your behalf to manage the resources that you use with the service. This role gives the clusters the permission to make calls to other AWS services. This is why we call this the *service role*.

Go to the [IAM Console](https://console.aws.amazon.com/iam/) and log in using your AWS credentials. Once logged on, in the navigation panel on the left, click on **Access management > Roles**:

<img src="images/aws/15-click-accessmgmt-roles.png" width="20%" height="20%">

> **NOTES:** the following lines assume this is the first time you're creating this service role. If you have already created one, please follow the section titled *Check for an existing cluster role* [instructions](https://docs.aws.amazon.com/eks/latest/userguide/service_IAM_role.html) to ensure that it has the correct permissions in it.

On the next screen, click on the *Create role* blue button:

<img src="images/aws/16-click-on-create-role.png" width="50%" height="50%">

On the **Create role** screen, click on *EKS*:

<img src="images/aws/17-click-on-eks.png" width="50%" height="50%">

This will expand the bottom of the screen to show you all the options you have related to EKS:

<img src="images/aws/18-select-eks-cluster-click-on-next-perms.png" width="50%" height="50%">

Click on the *EKS - Cluster* entry, which will enable the *Next: Permissions* blue button at the bottom. Click on this button to go to the next step:

<img src="images/aws/19-click-on-next-tags.png" width="50%" height="50%">

The screen above shows the defaulted attached permission policies for this role. The single policy *AmazonEKSClusterPolicy* is all one we need. Click on the *Next: Tags* blue button at the bottom.

<img src="images/aws/20-add-tag-click-review.png" width="50%" height="50%">

Although optional, it's always useful to add a tag so that you can search for AWS resources you created. You can enter any key-value pair you want but I always like to enter the *gitlabuser* as the **Key** and my GitLab handle as the **Value**. In the screenshot above, you would need to replace the string **YOUR GITLAB ID** with your own GitLab handle. Click on the *Next: Review* blue button:

<img src="images/aws/21-name-role-click-create.png" width="50%" height="50%">

Enter the name *eksClusterRole* in the **Role name** field above. Click on the *Create role* blue button at the bottom to create the role.

## Creating a VPC for your EKS cluster

Before you create your EKS cluster, you will need to create a VPC using AWS templates. You can reuse a VPC that you may already have but it needs to fulfill a variety of requirements. It's best to go ahead and create a new VPC for the EKS cluster you're about to create. You can create a VPC with only private subnets, only public subnets, or a combination of both, depending on your network requirements. For this lab, you will create a VPC with only public subnets.

> **NOTE:** You can also use the [AWS detail instructions](https://docs.aws.amazon.com/eks/latest/userguide/create-public-private-vpc.html) to create a VPC for your EKS cluster

Using your AWS credentials, log in to the [AWS CloudFormation console](https://console.aws.amazon.com/cloudformation/). From the navigation bar at the top right of your screen, select a region that supports Amazon EKS:

> **NOTE:** To see where EKS is available, check [here](https://docs.aws.amazon.com/general/latest/gr/eks.html).

<img src="images/aws/28-select-region.png" width="20%" height="20%">

For this lab, Ohio was selected but you can select any other region that supports EKS.

Expand the side menu (three horizontal lines) located on the left hand side of the screen:

<img src="images/aws/29-expand-left-menu.png" width="40%" height="40%">

Once expanded, click on **Stacks**:

<img src="images/aws/30-click-on-stacks.png" width="40%" height="40%">

On the **Stacks** window, click on the *Create stack* menu and select *With new resources (standard)*:

<img src="images/aws/31-click-on-with-new-resources.png" width="75%" height="75%">

On the **Create stack** window, ensure that the *Template is ready* and *Amazon S3 URL* tiles are selected. In the field **Amazon S3 URL**, paste the following URL:

> https://s3.us-west-2.amazonaws.com/amazon-eks/cloudformation/2020-10-29/amazon-eks-vpc-sample.yaml

Your screen should look as follows:

<img src="images/aws/32-paste-template-url.png" width="75%" height="75%">

Click on the **Next** button at the bottom of the screen to continue to the next step.

On the **Specify stack details** window, in the *Stack name* field at the top, enter the name for you VPC stack. In the screenshot below, you can replace the *[YOUR-GITLAB-ID]* with your GitLab handle and then append the suffix *-eks-vpc* to it. Leave the rest of the fields with their default values:

<img src="images/aws/33-enter-vpc-stack-name.png" width="75%" height="75%">

Click on the **Next** button at the bottom of the screen to continue to the next step.

On the **Configure stack options** window, although optional, it's always useful to add a tag to this resource so that you can search for AWS resources you created. You can enter any key-value pair you want but I always like to enter the *gitlabuser* as the **Key** and my GitLab handle as the **Value**. In the screenshot below, you would need to replace the string **YOUR GITLAB ID** with your own GitLab handle:

<img src="images/aws/34-enter-tag.png" width="75%" height="75%">

Leave the rest of the fields with their default values, scroll down to the bottom of the window and click on the **Create stack** button. 

<img src="images/aws/35-click-create-stack.png" width="75%" height="75%">

You will be put in the *Stacks* window that will show you the progress of the creation of your VPC. Once, the creation of the VPC is completed, you will see the message *CREATE_COMPLETE* under the name of your VPC on the left hand side of your screen. Your screen should look similar to this (I named my VPC *csaavedra-eks-vpc*, yours will be different):

<img src="images/aws/36-vpc-created-click-outputs.png" width="75%" height="75%">

On the right hand side, click on the *Outputs* tab to get the names of the VPC, security group, and subnets:

<img src="images/aws/37-vpc-outputs.png" width="75%" height="75%">

Copy and save the values of the SecurityGroup, VpcId, and SubnetIds to a file on your local machine (mine are shown above but yours will be different). You will need these for the creation of the EKS Cluster from GitLab.

Now, click the Back button on your browser to go back to the lab that called this page.
