# Examples for setting DNS CNAME record

We go over some examples on how to set DNS CNAME records with other domain name providers.

## Bluehost.com

If using Bluehost.com, here are their instructions on how to [create a CNAME (alias) record](https://my.bluehost.com/hosting/help/resource/714).

Here's a screenshot of the CNAME record creation window aliasing **\*.iteam-usa.org** to a very long alpha-numeric URL for the EKS Ingress. Notice that this input window is under the **iteam-usa.org** domain so there's no need to include the domain name **iteam-usa.org** in the **Host Record** field because it is assumed.

---

<img src="images/gitops/37-bluehost-create-wildcard-DNS.png" width="40%" height="40%">

---

## ionos.com

If using ionos.com, here are their instructions on how to [create a CNAME (alias) record](https://www.ionos.com/help/domains/configuring-cname-records-for-subdomains/configuring-a-cname-record-for-a-subdomain/). However, their instructions a somewhat outdated so we have updated them in the following list:

You can configure the CNAME record right in the Domains & SSL section of the Control Panel.

1. Log in to [IONOS](https://my.ionos.com) and go to the Domains & SSL section.

2. Next to the desired subdomain, click on the gear symbol under Actions and select DNS.

3. Click ADD RECORD and select CNAME.

4. Enter the desired subdomain in the Hostname field. Examples: www or shop

5. In the Point to field, type the desired entry (alias), specifying the Full Qualified Hostname. This is given without the part http:// or https:// - such as example.com.

6. Optional: Select the desired TTL.

The following is a screenshot of an example aliasing **\*.csaavedra.gitlabworkshops.io** to a very long alpha-numeric URL for the EKS Ingress. Notice that this input window is under the **gitlabworkshops.io** domain so there's no need to include the domain name **gitlabworkshops.io** in the **Host Name** field because it is assumed:

---

<img src="images/gitops/64-ionos-create-wildcard-DNS.png" width="40%" height="40%">

---

7. Click Save.

## GCP Cloud DNS

If you're managing your domains via a Google Cloud DNS public zone, then here are their instructions on how to [create a CNAME (alias) record](https://cloud.google.com/dns/docs/quickstart#create_a_cname_record_for_the_www_subdomain).

The following is a screenshot of an example aliasing **\*.csaavedra.tanuki.host** to a very long alpha-numeric URL for the EKS Ingress. Notice that this input window is under the **tanuki.host** domain so the domain name **tanuki.host** in the **DNS Name** field is readonly and greyed out:

---

<img src="images/gitops/65-GCP-create-wildcard-DNS.png" width="40%" height="40%">

---

## AWS Route53

If using AWS Route53, here are their instructions on how to [create an alias record](https://aws.amazon.com/premiumsupport/knowledge-center/route-53-create-alias-records/).

## A practical blog post

A good blog post that includes how to set up a DNS alias record can be found [here](https://about.gitlab.com/blog/2020/05/05/deploying-application-eks/).
