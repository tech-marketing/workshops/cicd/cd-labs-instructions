## Creating a project called simple-java

In this lab, you will go through steps to create a project called *simple-java*.

### Creating the project

1. Log on to your GitLab workspace. The first time you log in, you should see the following screen. Click on *Create a project*.

> **NOTE:** if this is not the first time you log in to GitLab workspace, head to your **Projects** window and click on the *New Project* blue button on the top right of your screen.

<img src="images/ado/ado-1-create-project.png" width="50%" height="50%">

2. On the next screen, click on *Import project*.

<img src="images/ado/ado-2-import-project.png" width="50%" height="50%">

3. On the next screen, click on *Repo by URL*.

<img src="images/ado/ado-3-import-from.png" width="75%" height="75%">

The screen will expand and show fields to populate for the import of a project via its URL.

<img src="images/ado/ado-4-enter-proj-info.png" width="50%" height="50%">

4. Enter the following project URL in the field **Git repository URL** field:

> https://gitlab.com/tech-marketing/sandbox/simple-java.git

and ensure that *Public* is checked for **Visibility Level**. Your screen should look similar to the following one:

<img src="images/ado/ado-5-enter-proj-URL-filled-out.png" width="50%" height="50%">

5. Next, click on the *Create project* button. You will see the following message for a few seconds while the project is being imported:

<img src="images/ado/ado-6-import-in-progress.png" width="50%" height="50%">

After a few seconds, the screen above will clear and you will be put into the newly created project:

<img src="images/ado/ado-7-proj-imported.png" width="50%" height="50%">

### Create a project-level EKS cluster

> **NOTE:** if you have already created a group-level EKS cluster for all your lab projects, then SKIP THE REST OF THIS LAB!

6. On the left navigation menu of your project window, click on **Infrastructure > Kubernetes clusters** to go to the Kubernetes screen:

---

<img src="images/auto-deploy/7-K8s-screen.png" width="50%" height="50%">

---

7. Click on the *Integrate with a cluster certificate* green button to go to the **Add a Kubernetes cluster integration** screen:

---

<img src="images/auto-deploy/8-click-on-create-EKS.png" width="75%" height="75%">

---

8. On this screen you can see tiles for GitLab's integrated cluster creation options. Since this lab is about creating an EKS cluster, click on the **Amazon EKS** tile to expand the screen with the input fields needed by the integration for the creation of an EKS cluster:

---

<img src="images/auto-deploy/9-enter-your-role-ARN.png" width="75%" height="75%">

---

9. In the above screen, you will see the default **Account ID** and **External ID**, which are readonly. You need to enter your **Provision Role ARN** (the string *arn:aws:iam::[AWS ACCOUNT]:role/[IAM USER]* is just an example of its syntax and not a real Provision Role ARN). Besides the **Provision Role ARN**, you will also need a **Service role**, **Key pair name**, and **VPC** with its subnets and security group. To create all these, follow these [instructions](subchapters/create-aws-roles-for-eks.md).

10. If you're back at this point, it means that you have created all the AWS resources needed by the GitLab integrated EKS cluster creation. So, enter your provision role ARN in the field **Provision Role ARN**. For the **Cluster region** field, enter the same region you used for the creation of the required cluster **VPC**, or you can leave it blank if you created your **VPC** in the us-east-2 (Ohio) region, which is the default for this field.

11. Click on the *Authenticate with AWS* button at the bottom of the screen. This will take you to the **Enter the details for your Amazon EKS Kubernetes cluster** window. On this window, enter the following values:

| Field | Value | Notes
| ----------- | ----------- | ----------- |
| Kubernetes cluster name | [YOUR GITLAB ID]-simple-java-eks-cluster | you can name it whatever you'd like but we recommend adding your GitLab handle in front so that you can easily find your cluster during a search. Replace *[YOUR GITLAB ID]* with your GitLab handle
| Environment scope | * | indicates that this cluster will be used for all environments you create within this project
| Kubernetes version | leave its default value
| Service role | eksClusterRole | if you followed the instructions, this should be the name for the service role
| Cluster Region | readonly and cannot change
| Key pair name | enter the one you created | if you followed the instructions, this should be *[YOUR GITLAB ID]-key-pair*
| VPC | enter the one you created | click on the field to open the search and type in the name of the VPC you created. If you followed the instructions, this should be *[YOUR GITLAB ID]-eks-vpc-VPC*
| Subnets | the three subnets you created | click on the field to open the search and the list of all your subnets. Select the ones you created one at a time and ensure that all three are entered in this field
| Security group | the name of the security group you created | click on the field to open the search and the list of all your security groups. If you followed the instructions, you should see a security group name *[YOUR GITLAB ID]-eks-vpc-ControlPlaneSecurityGroup-[long alphanumeric string]* in the list. Select this one. If you're not sure which security group, go to your **AWS Console > VPC Dashboard > Security > Security Groups** and locate the **Security group ID** for the security group you created. Its name will be in the column **Security group name**
| Instance type | m5.large | 2 vCPUs and 8GB RAM
| Number of nodes | 3 | good number for 1 master node and 2 worker nodes
| GitLab-managed cluster | checked | we want GitLab to manage this cluster
| Namespace per environment | checked | indicates that every environment will use its own namespace

12. Once you have populated all the fields in the **Enter the details for your Amazon EKS Kubernetes cluster** window, scroll to the bottom of the screen and click on the *Create Kubernetes Cluster* green button:

---

<img src="images/auto-deploy/38-click-on-create-cluster.png" width="75%" height="75%">

---

You will be put in the **Project cluster** window and you will see the following message while the EKS cluster is being created:

---

<img src="images/auto-deploy/39-cluster-being-created.png" width="30%" height="30%">

---

After 10-15 minutes, the creation process will finish and your newly created cluster window will be displayed:

> **NOTE:** In the next sample screen shots below, you will see that the cluster name is *csaavedra-eks-cluster* but yours will be *[YOUR GITLAB ID]-simple-java-eks-cluster*. You will also see a subgroup named *cd-workshop* but yours will be either your GitLab workspace OR an enclosing subgroup of your choice.

---

<img src="images/auto-deploy/69-eks-cluster-created.png" width="50%" height="50%">

---

13. We now need to create a project that can be designated as the management project for the newly created Kubernetes cluster. A management project can be used to run deployment jobs with Kubernetes cluster-admin privileges. We will use this project to install cluster-wide applications, e.g. Ingress, certmanager, Prometheus, into your cluster; see [management project template](https://docs.gitlab.com/ee/user/clusters/management_project_template.html) for details. For project-level clusters, the management project must be in the same namespace (or descendants) as the cluster’s project. Head over to the parent location where you created the project from the previous steps. This will be either the top level of your GitLab workspace OR an enclosing subgroup. In the sample screen shots below, we are using an enclosing subgroup *cd-workshop* but yours will have a name of your choosing. If you're creating the new cluster management project at the top level of your GitLab workspace, then skip to step 15 below. For the case of an enclosing subgroup, click on the name of the subgroup, *cd-workshop* (remember your enclosing subgroup will be different) in the top left side navigation menu:

---

<img src="images/auto-deploy/70-click-on-cd-workshop.png" width="10%" height="10%">

---

14. This will display the contents of the group. Click on the *New project* button on the right side of your screen:

---

<img src="images/auto-deploy/71-click-on-new-proj.png" width="75%" height="75%">

---

15. On the **Create new project** window, click on the *Create from template* tile:

---

<img src="images/auto-deploy/72-click-on-create-from-template.png" width="50%" height="50%">

---

16. On the templates window, scroll down until you see an entry for **GitLab Cluster Management** and then click on its *Use template* button:

---

<img src="images/auto-deploy/73-click-on-use-template.png" width="75%" height="75%">

---

17. On the *Create from template** window, enter **cluster-management** in the **Project name** field, select *Public* for **Visibility Level** and then click on the *Create project* button at the bottom of your screen:

---

<img src="images/auto-deploy/74-click-on-create-proj.png" width="50%" height="50%">

---

18. This will create a new project, *cluster-management*, inside of the *cd-workshop* group:

---

<img src="images/auto-deploy/75-cluster-mgmt-proj.png" width="75%" height="75%">

---

19. Now, we need to head back to your EKS cluster. Click on the entry *cd-workshop* in the breadcrumb at the top of your screen:

---

<img src="images/auto-deploy/76-click-on-breadcrumb-cd-workshop.png" width="30%" height="30%">

---

20. Once on the *cd-workshop* group, click on *Kubernetes* in the left navigation menu:

---

<img src="images/auto-deploy/77-click-on-K8s.png" width="10%" height="10%">

---

21. Click on the name of your EKS cluster (mine is *csaavedra-eks-cluster* but yours will be different):

---

<img src="images/auto-deploy/78-click-on-your-cluster-link.png" width="50%" height="50%">

---

22. On your EKS cluster detailed window, click on the *Advanced Settings* tab:

---

<img src="images/auto-deploy/79-click-on-advanced-settings.png" width="40%" height="40%">

---

23. In the section **Cluster management project**, click on the downarrow on the right side of the field and select the *cluster-management* project you created in step 16 above:

---

<img src="images/auto-deploy/80-select-cluster-mgmt-proj-and-save.png" width="75%" height="75%">

---

24. Now, we need to head back to the *cluster-management* project. To do this, click on the name of the project *cd-workshop* on the top left of your screen to show the contents of the group, which is just the single project *cluster-management*. Click on *cluster-management*:

---

<img src="images/auto-deploy/81-click-cd-workshop-and-cluster-mgmt.png" width="75%" height="75%">

---

25. At this point, you will see the contents of the project *cluster-management*. Click on the file *helmfile.yaml* to display its contents:

---

<img src="images/auto-deploy/82-click-on-helmfile-yaml.png" width="75%" height="75%">

---

26. Proceed to edit the *hemlfile.yaml* file by clicking on the *Edit* button. Once on the **Edit file** window, uncomment the lines for the ingress, cert-manager, and prometheus and then click on the *Commit changes* button at the bottom. Your changes should look as follows:

---

<img src="images/auto-deploy/83-edit-helmfile-uncomment-lines-click-commit.png" width="50%" height="50%">

---

27. This update will cause a pipeline to be started. Let's go and check the pipeline execution. Click on **CI/CD > Pipelines** in the left navigation menu:

---

<img src="images/auto-deploy/84-click-cicd-pipelines.png" width="30%" height="30%">

---

28. You will see a running pipeline as follows:

---

<img src="images/auto-deploy/85-pipeline-running.png" width="75%" height="75%">

---

29. Click on the pipeline number to see its detailed view. Wait until the pipeline finishes. When it's finished, it should look like this:

---

<img src="images/auto-deploy/86-pipeline-finished.png" width="40%" height="40%">

---

The pipeline above deploys prometheus, cert-manager and ingress to your EKS cluster. Now that you have an ingress up and running, you need to get the ingress IP. Let's do that now.

30. Open a terminal window on your laptop/desktop, and add an entry to your local kubeconfig so that you can execute kubectl commands. Here is the command you need to execute on your local terminal window to add the entry to your local kubeconfig:

> aws eks --region us-east-2 update-kubeconfig --name [your EKS cluster name] --role-arn [your provision role ARN]

Here's an example using my credentials:

---

<img src="images/auto-deploy/87-add-cluster-to-kubeconfig-redacted.png" width="75%" height="75%">

---

31. Now you can run the following kubectl command to get the ingress IP address:

> kubectl --namespace gitlab-managed-apps get services -o wide -w ingress-nginx-ingress-controller

**NOTE:** The command above behaves like a *tail* command so make sure you break out of it by hitting CTRL-C on your keyboard once you obtain the ingress IP address

Copy the value under the *EXTERNAL-IP* header, this is the ingress IP address.

Here's an example of my ingress IP address (yours will be different):

---

<img src="images/auto-deploy/88-getting-ingress-IP.png" width="75%" height="75%">

---

32. You now need to create a DNS CNAME (alias) record for this ingress endpoint with a public DNS service. I used Google Cloud DNS but you can use any other one, e.g. AWS Route53. You need to create two CNAME records, one for the domain and one for the wildcard domain. I'm using the domain **tmm.tanuki.host**, which I already own (the domain that you own will be different). Here's a screenshot of the domain CNAME record creation:

---

<img src="images/auto-deploy/43-create-DNS-entry.png" width="40%" height="40%">

---

and here's a screenshot of the wildcard domain CNAME record creation:

---

<img src="images/auto-deploy/44-create-wildcard-DNS-entry.png" width="40%" height="40%">

----
**NOTE:** You can find more examples of how to set DNS CNAME (alias) records with different cloud DNS services [here](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/subchapters/examples-for-setting-DNS-CNAME-record.md).

33. The last thing you need to do is to set your newly create EKS cluster base domain. Back on the EKS cluster detail window, click on the *Details* tab:

---

<img src="images/auto-deploy/45-set-base-domain.png" width="50%" height="50%">

---

34. In the *Base domain* field above, paste the public domain you used in your CNAME record. In my case, it was *tmm.tanuki.host*:

That's it! The EKS cluster is not all set to be used for deployments in your project.

Click [here](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/01.%20Auto%20DevOps.md#creating-a-project-and-enabling-auto-devops) to continue with this lab.
