# Incremental rollouts

An incremental rollout is one that deploys an updated application to production in incremental batches. For example, if you have ten instances of an application running in production, you can first update 10% of the instances (one instance) and monitor its behavior, if all goes well, you can then proceed to update 25% of the instances (two instances); if all continues well, you can continue to update 50% of the instances (five instances). Lastly, if 50% of the updated application instances are running well, then you can choose to update all of the applications instances in production. During an incremental rollout, if problems are encountered with the updated application instances, you can choose to stop the rollout and roll the updated application instances back to their working state.

## Benefits of incremental rollouts

Some of the benefits of rolling out to production in an incremental fashion are:

- It decreases the risk of a production outage or downtime leading to a better end use experience.
- Error rates or performance degradation can be monitored, and if there are no problems, all of production can be updated.
- Error rates or performance degradation can be monitored, and if there are problems, the rollout can be stopped without affecting all of production.
- It can help you improve your [DORA metric "Change failure rate"](https://cloud.google.com/blog/products/devops-sre/using-the-four-keys-to-measure-your-devops-performance)

## Enabling incremental rollouts

> **NOTE:** This subsection explains how to enable incremental rollouts and it is for your reference. **DO NOT** add the variable *INCREMENTAL_ROLLOUT_MODE* to your project at this point. We will be doing this later in the lab.

If you are using Auto DevOps, you can enable incremental rollouts as follows:

- Add the variable *INCREMENTAL_ROLLOUT_MODE* in the **variables:** section of your *.gitlab-ci.yml file*:

> INCREMENTAL_ROLLOUT_MODE: manual

If you'd like the incremental rollout to be timed, where each increment is rolled out with a one minute time in between, then add instead:

> INCREMENTAL_ROLLOUT_MODE: timed

- Define the following variable in your GitLab project or group:

---

<img src="images/rollout/1-incremental-var-dialog.png" width="50%" height="50%">

---

## Incremental rollout in action

Let's see how to implement manual and timed incremental rollouts in the *.gitlab-ci.yml file* for your project.

Your GitLab environment has been provisioned with a running Kubernetes cluster. At the end of the [previous lab](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/05.%20Canary%20Deployments.md), you increased the number of production pods to 10. Let's keep that number of production pods since it will help us to better visualize the incremental rollout.

1. First, let's delete two variables you created in the previous lab that we no longer need for this lab. Go to **Settings > CI/CD** and delete the variables *CANARY_ENABLED* and *CANARY_PRODUCTION_REPLICAS*.

2. Next, click on the name of the project **Simple Java** at the top of the left vertical navigation menu to display the contents of your project.

3. Create file .gitlab-ci.yml in the project and copy and paste the following text to it:

```
image: alpine:latest

variables:
  # KUBE_INGRESS_BASE_DOMAIN is the application deployment domain and should be set as a variable at the group or project level.
  # KUBE_INGRESS_BASE_DOMAIN: domain.example.com

  POSTGRES_ENABLED: "false"
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""  # https://gitlab.com/gitlab-org/gitlab-runner/issues/4501
  ROLLOUT_RESOURCE_TYPE: deployment
  INCREMENTAL_ROLLOUT_MODE: manual

stages:
  - build
  - production
  - incremental rollout 10%
  - incremental rollout 25%
  - incremental rollout 50%
  - incremental rollout 100%

build:
  stage: build
  image: "registry.gitlab.com/gitlab-org/cluster-integration/auto-build-image:v0.4.0"
  variables:
    DOCKER_TLS_CERTDIR: ""
  services:
    - docker:19.03.12-dind
  script:
    - |
      if [[ -z "$CI_COMMIT_TAG" ]]; then
        export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG}
        export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_SHA}
      else
        export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE}
        export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_TAG}
      fi
    - /build/build.sh
  rules:
    - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'

.auto-deploy:
  image: "registry.gitlab.com/gitlab-org/cluster-integration/auto-deploy-image:v1.0.7"
  dependencies: []

.production: &production_template
  extends: .auto-deploy
  stage: production
  script:
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy initialize_tiller
    - auto-deploy create_secret
    - auto-deploy deploy
    - auto-deploy delete rollout
    - auto-deploy persist_environment_url
  environment:
    name: production
    url: http://$CI_PROJECT_PATH_SLUG.$KUBE_INGRESS_BASE_DOMAIN
  artifacts:
    paths: [environment_url.txt, tiller.log]
    when: always

production:
  <<: *production_template
  rules:
    - if: '$CI_KUBERNETES_ACTIVE == null || $CI_KUBERNETES_ACTIVE == ""'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_MODE'
      when: never
    - if: '$CI_COMMIT_BRANCH == "master"'

production_manual:
  <<: *production_template
  allow_failure: false
  rules:
    - if: '$CI_KUBERNETES_ACTIVE == null || $CI_KUBERNETES_ACTIVE == ""'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_MODE'
      when: never

# This job implements incremental rollout on for every push to `master`.

.rollout: &rollout_template
  extends: .auto-deploy
  script:
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy initialize_tiller
    - auto-deploy create_secret
    - auto-deploy deploy rollout $ROLLOUT_PERCENTAGE
    - auto-deploy scale stable $((100-ROLLOUT_PERCENTAGE))
    - auto-deploy persist_environment_url
  environment:
    name: production
    url: http://$CI_PROJECT_PATH_SLUG.$KUBE_INGRESS_BASE_DOMAIN
  artifacts:
    paths: [environment_url.txt, tiller.log]
    when: always

.manual_rollout_template: &manual_rollout_template
  <<: *rollout_template
  stage: production
  resource_group: production
  allow_failure: true
  rules:
    - if: '$CI_KUBERNETES_ACTIVE == null || $CI_KUBERNETES_ACTIVE == ""'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_MODE == "timed"'
      when: never
    - if: '$CI_COMMIT_BRANCH != "master"'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_MODE == "manual"'
      when: manual

.timed_rollout_template: &timed_rollout_template
  <<: *rollout_template
  rules:
    - if: '$CI_KUBERNETES_ACTIVE == null || $CI_KUBERNETES_ACTIVE == ""'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_MODE == "manual"'
      when: never
    - if: '$CI_COMMIT_BRANCH != "master"'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_MODE == "timed"'
      when: delayed
      start_in: 1 minutes

timed rollout 10%:
  <<: *timed_rollout_template
  stage: incremental rollout 10%
  variables:
    ROLLOUT_PERCENTAGE: 10

timed rollout 25%:
  <<: *timed_rollout_template
  stage: incremental rollout 25%
  variables:
    ROLLOUT_PERCENTAGE: 25

timed rollout 50%:
  <<: *timed_rollout_template
  stage: incremental rollout 50%
  variables:
    ROLLOUT_PERCENTAGE: 50

timed rollout 100%:
  <<: *timed_rollout_template
  <<: *production_template
  stage: incremental rollout 100%
  variables:
    ROLLOUT_PERCENTAGE: 100

rollout 10%:
  <<: *manual_rollout_template
  variables:
    ROLLOUT_PERCENTAGE: 10

rollout 25%:
  <<: *manual_rollout_template
  variables:
    ROLLOUT_PERCENTAGE: 25

rollout 50%:
  <<: *manual_rollout_template
  variables:
    ROLLOUT_PERCENTAGE: 50

rollout 100%:
  <<: *manual_rollout_template
  <<: *production_template
  allow_failure: false
```

The template above defines the stages and jobs to automatically build the project and incrementally deploy the application to a running Kubernetes cluster either manually or in a timed fashion. Also notice that it is using the same variable that Auto DevOps uses to enable incremental rollouts, *INCREMENTAL_ROLLOUT_MODE*, which is being set to *manual* in the *variables:* section of the template above. 

4. Click on the Commit changes at the bottom left of your screen to commit your changes.

5. The commit will launch the execution of the pipeline. To see the updated pipeline that contains only Auto Build and the manual incremental rollout jobs under the Production stage, click on **CI / CD > Pipelines** and then click on the latest pipeline number to see the pipeline detail view.

> **NOTE:** If the *build* job fails the first time you run the pipeline, re-run the build job by clicking on its *Retry* button <img src="images/rollout/3-retry-icon.png" width="3%" height="3%">.

Once the build job has successfully completed, your pipeline should look like this:

---

<img src="images/rollout/4-manual-pipe-build-complete.png" width="40%" height="40%">

---

6. On the pipeline above, to manually start executing the incremental rollout, start by clicking on the Play button <img src="images/rollout/8-play-icon.png" width="3%" height="3%"> of the job *rollout 10%*. This will deploy your application to one of the ten pods in your Kubernetes cluster. Here's the way your pipeline should look at this point:

---

<img src="images/rollout/6-10-percent-job-done.png" width="40%" height="40%">

---

7. To check the production environment, click on **Operations > Environments**. You should see 1 pod in your production environment marked with a small orange circle indicating the 10% incremental deployment:

---

<img src="images/rollout/7-10-percent-deployed.png" width="75%" height="75%">

---

<!--
Next, go back to the pipeline view by clicking on the Back button on your browser. Then, click on the Play button of the job *rollout 25%* to deploy your application to two of the ten pods in your Kubernetes cluster. Here's the way your pipeline should look at this point:

---

<img src="images/rollout/9-25-percent-job-done.png" width="40%" height="40%">

---

Again, to check the production environment, click on **Operations > Environments**. This time, you should see 2 pods in your production environment marked with a small orange circle indicating the 20% incremental deployment:

---

<img src="images/rollout/10-25-percent-deployed.png" width="75%" height="75%">

---

-->

8. Go back to the pipeline view by clicking on the Back button on your browser. Then, click on the Play button of the job *rollout 50%* to deploy your application to five of the ten pods in your Kubernetes cluster. Here's the way your pipeline should look at this point:

---

<img src="images/rollout/11-50-percent-job-done.png" width="40%" height="40%">

---

9. To check the production environment, click on **Operations > Environments**. This time, you should see 5 pods in your production environment marked with a small orange circle indicating the 50% incremental deployment:

---

<img src="images/rollout/12-50-percent-deployed.png" width="75%" height="75%">

---

10. One last time, go back to the pipeline view by clicking on the Back button on your browser. Then, click on the Play button of the job *rollout 100%* to deploy your application to all of the ten pods in your Kubernetes cluster. Here's the way your pipeline should look at this point:

---

<img src="images/rollout/13-100-percent-job-done.png" width="40%" height="40%">

---

11. And finally, to check the production environment, click on **Operations > Environments**. This time, you should see the 10 pods in your production environment all green indicating the 100% incremental deployment:

---

<img src="images/rollout/14-100-percent-deployed.png" width="75%" height="75%">

---

The pipeline template above includes manual and timed incremental deployments to production. Let's go over what you need to do to exercise the timed incremental deployment.

## Timed rollout in action

The pipeline template above (the one you copied and pasted into your *.gitlab-ci.yml* file) also includes stages and jobs for a timed rollout. To exercise the timed rollout, where each increment is rolled out with a one minute time in between, you could modify the value of the variable *INCREMENTAL_ROLLOUT_MODE* (defined in the *variables:* section in your *.gitlab-ci.yml* file) to *timed* as follows:

> INCREMENTAL_ROLLOUT_MODE: timed

However, for this lab let's do something different. Since we know that project or group variables take precedence over variables defined in the pipeline file, instead of modifying the *.gitlab-ci.yml* file, let's define the the variable *INCREMENTAL_ROLLOUT_MODE* in your GitLab project or group and assign it the *timed* value.

12. To do this, click on **Settings > CI / CD**. On the Settings window, scroll down to the **Variables** section and click on its *Expand* button. Then click on the *Add Variable*, and proceed to add the variable INCREMENTAL_ROLLOUT_MODE to your project and assign the value *timed* to it. Your *Add Variable* dialog should look as follows:

---

<img src="images/rollout/15-timed-var-dialog.png" width="50%" height="50%">

---

13. Click on the *Add Variable* button in the dialog to add the variable to your project.

14. Since you didn't make any changes to files in your project, no pipeline will be launched as a result of adding this single variable to your project. So, you need to launch the pipeline manually. To do this, click on **CI / CD > Pipelines** to view the Pipelines window. On this screen, click on the *Run Pipeline* button on the top right to kick off the pipeline. You should see the following:

---

<img src="images/rollout/16-run-pipeline-manually.png" width="50%" height="50%">

---

14. Ensure that the **Run for branch name or tag** field says *master*. Then, click on the *Run Pipeline* button.

> **NOTE:** Instead of defining the variable INCREMENTAL_ROLLOUT_MODE at the project level, you could have defined it in the **Variables** section of the preceeding *Run Pipeline* screen. Defining it here has higher precendence than any project/group variables or YAML-defined variables.

Your screen will be refreshed to the detailed pipeline view. You should see the timed rollout pipeline as follows:

---

<img src="images/rollout/17-timed-rollout-pipeline.png" width="75%" height="75%">

---

Right after the *build* job finishes, each of the *timed rollout XX%* jobs will be automatically executed with a 1-minute interval in between each. At this point, you can wait for all the jobs to finish running, or you can just continue to the next lab. After all jobs have completed, your pipeline should look like this:

---

<img src="images/rollout/18-timed-rollout-complete.png" width="75%" height="75%">

---

Now that we have reviewed incremental rollouts, let's move on to the next lab to go over how to manually roll back and how to automatically roll back based on an alert.
